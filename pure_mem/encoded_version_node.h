//  Copyright (c) 2020-present,  INSPUR Co, Ltd.  All rights reserved.
// This source code is licensed under Apache 2.0 License.

// liliupeng@inspur.com

#pragma once

#include <atomic>

#include "abstract_version_node.h"

namespace rocksdb {
// The Node is a storage node used to store the kv data retrieved from the art
// tree, including a doubly linked list and kv data.
class EncodedVersionNode : public VersionNode {
private:
  std::atomic<void*> version;

public:
  EncodedVersionNode() : VersionNode(VersionNode::NodeType::ENCODED) {
    version.store(nullptr, std::memory_order_relaxed);
  }

  const char* Key() const override {
    return reinterpret_cast<const char*>(version.load());
  }

  void GetMvccKey(MvccKey& mk, Slice& key) override {
    mk.parseKey(key);
  }

  void GetInternalKey(Slice& key, const char* buf) override {
    key = GetLengthPrefixedSlice(buf);
  }

  void GetValue(const Slice& key, Slice& value) override {
    value = GetLengthPrefixedSlice(key.data() + key.size());
  }

  bool CASSetKey(void *expected, void *x) {
    return version.compare_exchange_strong(expected, x);
  }

  void GetSeqAndType(uint64_t seqNumAndType, uint64_t* seq, ValueType* t) override {
    UnPackSequenceAndType(seqNumAndType, seq, t);
  }
};

}  // namespace rocksdb
