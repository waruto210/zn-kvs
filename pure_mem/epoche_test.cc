// Copyright (c) 2020-present, INSPUR Co, Ltd. All rights reserved.
// This source code is licensed under Apache 2.0 License.

#include <iostream>
#include "pure_mem/epoche.h"
#include "rocksdb/env.h"
#include "util/concurrent_arena.h"
#include "util/hash.h"
#include "util/random.h"
#include "util/testharness.h"

using namespace rocksdb;
class EpocheTest : public testing::Test {
};

TEST_F(EpocheTest, testMarkDel) {
    
    assert(DeleteWhileNoRefs::getInstance()->getDeleteNum() == 0);
    for (int i = 0; i< 10; i++){
        char* a = new char[10];
        DeleteWhileNoRefs::getInstance()->markNodeForDeletion(a);
    }
    assert(DeleteWhileNoRefs::getInstance()->getDeleteNum() == 10);
    
    std::this_thread::sleep_for(std::chrono::seconds(5));

    assert(DeleteWhileNoRefs::getInstance()->getDeleteNum() == 0);
    for (int i = 0; i< 10; i++){
        char* a = new char[10];
        DeleteWhileNoRefs::getInstance()->markNodeForDeletion(a);
    }
    assert(DeleteWhileNoRefs::getInstance()->getDeleteNum() == 10);

    std::this_thread::sleep_for(std::chrono::seconds(5));

    assert(DeleteWhileNoRefs::getInstance()->getDeleteNum() == 0);

}

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
