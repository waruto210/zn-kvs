// Copyright (c) 2020-present, INSPUR Co, Ltd. All rights reserved.
// This source code is licensed under Apache 2.0 License.

#pragma once

#include <limits>
#include "include/rocksdb/slice.h"

namespace syn_art_fullkey {
/**
 *　接口类，用于封装从叶子节点加载出ｋｅｙ的函数
 * 上层逻辑决定叶子节点的内容，因此，需要上层类集成实现该函数
 */
class ILoadKey {
public:
  // 析构函数，默认需要，不然编译时会告警
  virtual ~ILoadKey() {}
  // 从叶子节点ＴＩＤ中加载出当前叶子节点对应的ｋｅｙ函数
  virtual void parseTid2Key(void* tid, rocksdb::Slice &key) { assert(false); }
};

} // namespace art_rowex
