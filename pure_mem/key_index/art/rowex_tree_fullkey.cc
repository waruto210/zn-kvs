// Copyright (c) 2020-present, INSPUR Co, Ltd. All rights reserved.
// This source code is licensed under Apache 2.0 License.

#include "pure_mem/key_index/art/rowex_tree_fullkey.h"

namespace rocksdb {

RowexTreeFullKey::RowexTreeFullKey(LoadKeyFromValue load, Logger *logger) : ITree(load, logger) {
  tree_ = new syn_art_fullkey::Tree(this);
}

RowexTreeFullKey::~RowexTreeFullKey() { delete tree_; }

bool RowexTreeFullKey::insertNoReplace(const Slice &key, void *curNode,
                                void *&retNode) {
  return tree_->insertOrGetLG(key, curNode, retNode);
}
void *RowexTreeFullKey::getGT(const Slice &key) {
  return tree_->getLG(key);
}
void *RowexTreeFullKey::getGE(const Slice &key) {
  return tree_->getEG(key);
}

void RowexTreeFullKey::parseTid2Key(void* tid, Slice &key) {
  loadKey_(tid, key);
}

} // namespace rocksdb