# WAL管理模块

## (26/2/2021)

## 0. 简介

在内存引擎中，有大量数据保存在内存中，因此，WAL（预写日志文件）不再适合保存内存中所有的数据。为了能够让WAL与内存引擎适配，所以使用WAL管理模块负责预写日志文件管理工作。

## 1. WAL管理原因

- 防止单个 WAL日志文件过大。
- 删除已落盘数据对应的 WAL日志文件，防止恢复时耗时太久。
- 并发机制下，数据能够快速落盘。

## 2. WAL管理模块职责

- 首先将并发写入的多条数据合成一条数据，然后将这条数据写入WAL，以减少落盘次数，提高写入的性能。
- 在向WAL写入数据之前，获取WAL文件大小和上次定时落盘时间，如果超过文件阈值或间隔时间，关闭此WAL文件，创建下一个WAL文件并将这条数据写入新的WAL文件。
- 为了防止WAL日志文件过多导致恢复耗时太久，WAL管理模块需要定时清理那些数据已经落盘的WAL日志文件。判断数据是否已经落盘的依据是落盘模块的提供的信号量。
- 数据恢复中，首先恢复WAL日志文件，以确保最新的还未落盘数据能够正常恢复。

## 3. WAL写入机制

### 数据结构

如下图，一个 Writer 对象封装一个 WriteBatch 以及其对应的一些配置信息，如分配到的 SeqNumber、线程状态信息、是否写 WAL 日志、是否写入 Memtable 等。Writer 对象之间组成一个双向链表，新插入 WriteBatch 将封装为 Writer 结构插入到链表末尾，等待编入 WriteGroup 进行处理。

WriteGroup 由一组相邻的 Writer 构成，分为一个 Leader 和多个 Follower 。在一个 WriteGroup 里面，重要部分（WAL 日志写）甚至是所有的写操作（包括 WAL 日志写和Memtable 写）都由 Leader 完成。Leader 可以是第一个插入到空链表的 Writer，也可以是上一个 WriteGroup 执行结束以后获取到的链表头部的 Writer。Leader 将负责协调 WriteGroup 内写操作的有序执行，适时地唤醒 Follower，并在 WriteGroup 执行结束唤醒新的Leader 执行新一轮的 WriteGroup 写。

![组提交图](./images/组提交图.jpeg)

### 执行流程

在默认情况下：

1. 一个 WriteBatch 封装为 Writer 对象，插入等待链表当中。插入的同时判断是否插入空链表而被选为起始 Leader：若是，则执行步骤2；否则 Writer 线程进入等待状态等待唤醒，执行步骤6。
2. Leader 从等待链表中获取一批 Writer 组成 WriteGroup，其他 Writer 作为 Follower。
3. Leader 合并 WriteGroup 内所有 Writer 的数据，设置 seqNumber，写 WAL 日志。
4. Leader 修改 Follower 的状态，唤醒 Follower，Follower只执行自己的数据写入操作。 
5. Follower 检查状态，执行自己的数据写入操作并退出 WriteGroup。
6. Leader 等待 Follower 退出 WriteGroup，更新等待链表，尝试唤醒下一个Leader。

### 组提交方式在内存引擎的应用

将多条数据合并至一次WAL的写入，以减少写入磁盘的次数，可以极大提高数据库写入的性能。在内存引擎中，保留了组提交的写入方式。

在内存引擎中，多条数据合并为一条数据，由Leader线程负责写入WAL。之后，唤醒组内的Follower线程，多线程并发写入Memtable。待所有线程写入完Memtable后，公布Last Sequence，完成本次组的写入过程，并唤醒下一个Leader。我们不但简化数据的写入流程，而且保存了高效的组提交方式。

## 4. WAL管理模块与落盘模块

（1）WAL管理模块与落盘模块的目的都是数据库快速恢复。

（2）删除wal文件的时机

​     WAL文件删除的时机是落盘模块中，落盘队列中所有落盘节点全部落盘后的时间点为依据的。

（3）数据库的恢复

​     WAL管理模块的数据是内存块中的最新数据，在数据恢复过程中，首先恢复WAL文件的数据，根据WAL文件中的数据，优先恢复对应落盘节点的数据。