# Performence Test


## 1. Rocksdb  db_bench tool
Using  db_bench_tool in Rocksdb ,with supportting zn-kvs .

For test detail , see [here](db_benchmark.md)

## 2. CR storage engine bench tool
Using  storage engine bench tools in ZNBASE . first we should Integrate zn-kvs into ZNBASE. There are many introduction show how todo it.

For test detail , see [here](benchmark_CR.md)


## 3. CR TPCC tool
Using  TPCC tool in ZNBASE . first we should Integrate zn-kvs into ZNBASE. There are many introduction show how todo it.

For test detail , see [here](TPCC_CR.md)

